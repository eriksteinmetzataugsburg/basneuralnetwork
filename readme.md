This is a software package that allows for the training and evalutaion of neural networks on BAS geomagnetic data.

The software dependencies, or software which must be installed before this package may be used are:

python 3.x

tensorflow for python

keras for python

sklearn for python

numpy for python

Python Image Library (PIL) for python

Please ensure that these software packages are installed prior to attempting to use this software package.

The software package is comprised of source code and utilities, both of which are written in python, and 
several sets of labels for BAS geomagnetic data. The BAS geomagnetic data is not included with this repository;
the BAS geomagnetic data may be retrieved and parsed with the utilities included in this package.


### UTILITIES ###

The following utilities are included with this software package, and serve as methods of altering the BAS data.
The utilities are located in the directory "repositoryroot/utils/". Please note that if you are running this software
on bababge at Augsburg, the utilities have already been run.

If you want more details, each of the utilities is commented and contains a help option that may be invoked from
the command line using the option "-h" or "--help"

getData.py

	python getData.py --help

This utility **must** be run with the argument made to retrieve raw ascii data from BAS. It can also fetch
spectrograms from the BAS website, which may be used to create new data labels.

	sudo python getData.py --type ascii

computeTransforms.py

	python computeTransforms.py --help

This utility **must** be run with the argument for the input directory pointing to the location to which the raw
ascii files were downloaded. It can also be run with specification for the size of the sample window to compute
transforms on, and with specification for the distance to advance the sample window after each transform is computed.

	sudo python computeTransforms.py --input_directory BAS_Data/raw_ascii/

computeNonLogBuckets.py

	python computeNonLogBuckets.py --help

This utility should be run with the argument for the input directory pointing to the location to which the transforms
were stored. It can also be run with specification for the size of the buckets into which the transformed data is to be
placed. The bucket size equates out to the number of integer values which one bucket encompasses.

	sudo python computeNonLogBuckets.py --input_directory BAS_Data/parsed_ascii/transforms/

computeLogsAndLogBuckets.py

	python computeLogsAndLogBuckets.py --help

This utility may be run with the argument for the input directory pointing to the location to which the transforms 
were stored. It can also be run with the specification for the number of buckets into which the log base 10 of the
transformed data is to be placed. The bucket size is taken relative to the observed maximum value of transformed data
found in previous computations.

	python computeLogsAndLogBuckets.py --input_directory BAS_Data/parsed_ascii/transforms/

createLabels.py

	python createLabels.py --help

This utility may be run with the argument for the input directory pointing to the location to which the BAS
spectrograms were downloaded, and the output directory pointing to a location where you would like to store the labels.
A suggested output directory, assuming it has not already been created by another user, is shown below.

	python createLabels.py --input_directory BAS_Plots/images/ --output_directory BAS_Data/labels/set2/


### SOURCE CODE ###

There are two source files. One allows the user to train a neural network, the other allows for evaluation of previously
trained networks. Both require that BAS ascii be already downloaded and at the least transformed, if not also placed into 
buckets.


trainNetworks.py

	python trainNetworks.py --help

This is the core of this software package. It is designed such that users can easily train previously defined networks
using different training parameters. It also allows for users to train networks that they have defined on their own.

In order to train networks of the user's own definition, first observe how the files in the directory "src/cnn/" are
structured. Any new networks must contain a class named "convnet" which must contain a method named "build" with the
arguments width, height, and depth. These arguments must correspond to the input shape of the first layer in the network.
Outside of these two restrictions, you may define your network in any way you choose.

To train your network, specify the option networks with your file name as shown below:

	python trainNetworks.py --networks <yourfilenamehere> --label_sets set1,set0 
		--input_data BAS_Data/parsed_ascii/transforms/ --optimizer Adam

If you would like to retrain the network that was observed by johns116 as having the highest accuracy of the options
attempted by johns116, specify the option networks with the file name as shown below:

	python trainNetworks.py --networks convnet170705_1 --label_sets set1,set0
		--input_data BAS_Data/parsed_ascii/transforms/buckets/ --optimizer Adam

There are an infinite number of network architectures that may be trained, and there is an incredibly large number of
training parameters that may be varied. See the output of the --help option for more details, or read the comments in the
code along with the code itself.


makePredictions.py

	python makePredictions.py --help

This is the software that allows us to demonstrate the function of the networks. In order to evaluate the network that
was observed to have the highest accuracy of 97.16%, first run the getData.py, computeTransforms.py, and
computeNonLogBuckets.py utilities as described above, then run this software with the following arguments:

	python makePredictions.py --network convnet170705_1 --weights results/2017-07-27_run0/weights/convnet170705_1session1
		--input_data BAS_Data/raw_ascii/ --input_type BAS

This command will run through ALL of the BAS data and provide you with predictions for each day for which there is BAS data.


### ADDITIONAL INFO ###

For any questions, email johns116@augsburg.edu, or email steinmee@augsburg.edu and ask to have your message be forwarded 
to Matthew Johnson of class 2018.

Please note that if you wish to use different input data, there is documentation on the formatting of data files in the
BAS_Data/ directory.

It should also be noted that there is more software that performs the same functions as the software contained in this package
located on babbage at Augsburg, though the software is not nearly as user friendly. These old bits of software along with old
files relating to the software may be read if there is more information desired about the evolution of this software package. 
The lab notebook titled "Augsburg - URGO Summer 2017" might also be referred to.
