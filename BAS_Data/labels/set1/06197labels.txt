# data labels for day 06197
# one line for each x y and z components, one entry on each line for each hour of the day
# entries are an array of length 2, index 0 being false there is no anomaly, index 1 being true there is an anomaly
[2 2][0 1][2 2][1 0][1 0][1 0][1 0][1 0][1 0][1 0][1 0][2 2]
[0 1][0 1][2 2][1 0][1 0][1 0][1 0][1 0][1 0][1 0][1 0][2 2]
[2 2][0 1][2 2][1 0][1 0][1 0][1 0][1 0][1 0][1 0][1 0][2 2]