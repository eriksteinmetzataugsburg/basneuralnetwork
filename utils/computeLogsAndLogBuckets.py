# a program to take transformed data, compute the log base 10 of each transformed magnitude
# and place each of those log base 10 values into discrete buckets. write both the log base 10
# values and the buckets to file.

import os
import sys
import numpy as np
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("--input_directory", type=str, help="(required) the input directory of transformed values relative to repository root. ex: \"BAS_Data/parsed_ascii/transforms/\"")
ap.add_argument("--log_output_directory", type=str, help="(optional) the output directory for the log base 10 values relative to repository root. ex: \"BAS_Data/parsed_ascii/log10s/\", defaults to \"input_directory/log10s/\"")
ap.add_argument("--bucket_output_directory", type=str, help="(optional) the output directory for the bucketed/binned values relative to repository root. ex: \"BAS_Data/parsed_ascii/log10buckets/\", defaults to \"input_directory/log10buckets\"")
ap.add_argument("--num_buckets", type=int, default=512, help="(optional) the number of discrete buckets into which the log base 10 are to be divided. minumum of 2. defaults to 512.")
args = vars(ap.parse_args())

# ensure that an input directory is specified
if args["input_directory"] == None:
    print('error: no input directory specified')
    exit()

# get the parent file path
parentFilePath = os.path.abspath('')
pathDelims = ''
if '\\' in parentFilePath:
    pathDelims = '\\'
else:
    pathDelims = '/'
parentPathParts = parentFilePath.split(pathDelims)
parentFilePath = ''
for i in range(0, len(parentPathParts) -1):
    parentFilePath += parentPathParts[i] + pathDelims

# check that the input directory exists
inputDirectory = parentFilePath + args["input_directory"]
if not os.path.isdir(inputDirectory):
    print('error: ' + inputDirectory + ' was not a directory')
    exit()

# check that the log 10 output directory exists
if args["log_output_directory"] == None:
    log10Directory = inputDirectory + 'log10s' + pathDelims
    os.makedirs(log10Directory)
else:
    log10Directory = parentFilePath + args["log_output_directory"]
    if not os.path.isdir(log10Directory):
        print('error: ' + log10Directory + ' was not a directory')
        exit()

# check that the bucket output directory exists
if args["bucket_output_directory"] == None:
    bucketDirectory = inputDirectory + 'log10buckets' + pathDelims
    os.makedirs(bucketDirectory)
else:
    bucketDirectory = parentFilePath + args["bucket_output_directory"]
    if not os.path.isdir(bucketDirectory):
        print('error: ' + bucketDirectory + ' was not a directory')
        print('error: ' + bucketDirectory + ' was not a directory')
        exit()

# parse the argument for the number of buckets into which we will divide the log base 10 values
if args["num_buckets"] < 2:
    print('error: number of buckets specified was less than 2')
    exit()
numBuckets = args["num_buckets"]

# list the files in the input directory
filesFound = os.listdir(inputDirectory)
print("[INFO] found a total of " + str(len(filesFound)) + " files.")

# keep track of the files we have translated and loop through input files
numTranslated = 1
for file in filesFound:
    # check that the file is a text file
    if os.path.isfile(inputDirectory + file) and 'txt' in file.lower():
        print("[INFO] translating file " + file + ", number " + str(numTranslated) + " of " + str(len(filesFound)))
        # open the input file
        inputFile = open(inputDirectory + file)
        inputFile.readline()
        # open the output files
        logFile = open(log10Directory + file, 'w')
        logFile.write("# the log base 10s of the transformed data for file " + file + "\n")
        bucketFile = open(bucketDirectory + file, 'w')
        bucketFile.write("# the bucketed log base 10s of the transformed data for file " + file + "\n")
        linesRead = 0
        # start writing to the files
        for line in inputFile:
            dat = line.split()
            # write the timestamp for the data point
            if linesRead % 3 == 0:
                logFile.write("X" + str((linesRead//3)*300) + ": ")
                bucketFile.write("X" + str((linesRead//3)*300) + ": ")
            elif linesRead % 3 == 1:
                logFile.write("Y" + str((linesRead//3)*300) + ": ")
                bucketFile.write("Y" + str((linesRead//3)*300) + ": ")
            elif linesRead % 3 == 2:
                logFile.write("Z" + str((linesRead//3)*300) + ": ")
                bucketFile.write("Z" + str((linesRead//3)*300) + ": ")
            # write the data point
            for i in range(1, len(dat)):
                val = np.float(dat[i])
                val = np.absolute(val)
                if val < 1:
                    val = 0
                else:
                    val = np.log10(val)
                logFile.write(str(val) + " ")
                val = np.floor(val / (8.4 / numBuckets)) 
                bucketFile.write(str(val) + " ")
            logFile.write("\n")
            bucketFile.write("\n")
            linesRead = linesRead + 1
        logFile.close()
        bucketFile.close()
        inputFile.close()
    numTranslated = numTranslated + 1
