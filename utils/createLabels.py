# This is a program to create labels from BAS spectrographs
# The program presents 2 hour windows, which the user must then indicate as being
# an anomaly, non anomalous, or to discard the 2 hour window from the label set.

import os
import sys
import time
from PIL import Image
import numpy as np
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("--input_directory", type=str, help="(required) the input directory to take spectrographs from, relative to repository root. ex: \"BAS_Plots/images/\"")
ap.add_argument("--output_directory", type=str, help="(required) the output directory to write labels to, relative to repository root. ex: \"BAS_Data/labels/set2/\"")
args = vars(ap.parse_args())

# check if the arguments are empty
if args["input_directory"] == None:
	print('error: no input directory was specified')
	exit()
if args["output_directory"] == None:
	print('error: no output directory was specified')
	exit()

# get the parent file path
parentFilePath = os.path.abspath('')
pathDelims = ''
if '\\' in parentFilePath:
	pathDelims = '\\'
else:
	pathDelims = '/'
parentPathParts = parentFilePath.split(pathDelims)
parentFilePath = ''
for i in range(0, len(parentPathParts) - 1):
	parentFilePath += parentPathParts[i] + pathDelims

# check if the provided paths are directories
inputDirectory = parentFilePath + args["input_directory"]
if not os.path.isdir(inputDirectory):
    print('error: ' + inputDirectory + ' was not a directory')
    exit()
outputDirectory = parentFilePath + args["output_directory"]
if not os.path.isdir(outputDirectory):
    print('error: ' + outputDirectory + ' was not a directory')
    exit()

# unqueue any files for which there is already a label file in the output directory
allfiles = os.listdir(inputDirectory)
completedfiles = os.listdir(outputDirectory)
for file in completedfiles:
	allfiles.remove("hb" + file[0:5] + "_1hz.gif")
num = len(allfiles)

# track which file we are on
index = 0

# loop through the files
for file in allfiles:
	index = index + 1
	# if the file is a file and has gif in the name, open it and present it to the user
	if os.path.isfile(inputDirectory + file) and '.gif' in file:
		print("[INFO] on image " + file + " number " + str(index) + " of " + str(num))
		img = Image.open(inputDirectory + file)
		img.show()
		# get color mappings for pink, yellow, and white via the scale on the right side of spectrogram
		pinkValue = img.getpixel((725, 200))
		yellowValue = img.getpixel((725, 285))
		whiteValue = img.getpixel((0, 0))
		# let user label the whole day as non anomalous if they wish
		userInput = input("enter \' if there are no anomalies this day, enter anything else for an anomaly...")
		# open the output file
		outputFile = open(outputDirectory + file[2:7] + 'labels.txt', 'w')
		outputFile.write('# data labels for day ' + file[2:7] + '\n# one line for each x y and z components, one entry on each line for each hour of the day\n')
		outputFile.write('# entries are an array of length 2, index 0 being false there is no anomaly, index 1 being true there is an anomaly\n')
		# if the user didn't label whole day as non anomalous, proceed
		if userInput != '\'':
			# go through the x axis data first
			for i in range(0, 12):
				# write white into the space below the 2 hour window we just opened (this is for relabeling images that have already been labeled)
				for pxx in range ((86 + i*48), (132 + i*48)):
					for pxy in range (188, 197):
						img.putpixel((pxx, pxy), whiteValue)
				# crop the 2 hour window and present it to the user for inspection
				imgCrop = img.crop((85 + i*48, 36, 133 + i*48, 187))
				imgCrop.resize((288, 900)).show()
				# prompt the user for the label they wish to ascribe to the 2 hour window
				yesno = input('Did the image contain an anomaly? enter ; for yes \' for no or l for lose')
				while yesno != ';' and yesno != '\'' and yesno != 'l':
					yesno = input('Invalid input, please enter ; for yes or \' for no or l for lose')
				# if the answer yes, edit image and write true to file
				if yesno == ';':
					for pxx in range ((86 + i*48), (132 + i*48)):
						for pxy in range (188, 197):
							img.putpixel((pxx, pxy), pinkValue)
					dataPoint = np.array([0, 1])
					outputFile.write(str(dataPoint))
				# if the answer is no, write false to the file
				if yesno == '\'':
					dataPoint = np.array([1, 0])
					outputFile.write(str(dataPoint))
				# if the answer was lose, write something else to the file
				if yesno == 'l':
					for pxx in range ((86 + i*48), (132 + i*48)):
						for pxy in range (188, 197):
							img.putpixel((pxx, pxy), yellowValue)
					dataPoint = np.array([2, 2])
					outputFile.write(str(dataPoint))
			outputFile.write("\n")
			# then go through the y axis data
			for i in range(0, 12):
				# write white into the space below the 2 hour window we just opened (this is for relabeling images that have already been labeled)
				for pxx in range ((86 + i*48), (132 + i*48)):
					for pxy in range (376, 385):
						img.putpixel((pxx, pxy), whiteValue)
				# crop the 2 hour window and present it to the user for inspection
				imgCrop = img.crop((85 + i*48, 224, 133 + i*48, 374))
				imgCrop.resize((288, 900)).show()
				# prompt the user for the label they wish to ascribe to the 2 hour window
				yesno = input('Did the image contain an anomaly? enter ; for yes \' for no or l for lose')
				while yesno != ';' and yesno != '\'' and yesno != 'l':
					yesno = input('Invalid input, please enter ; for yes or \' for no or l for lose')
				# if the answer was yes, edit image and write true to file
				if yesno == ';':
					for pxx in range ((86 + i*48), (132 + i*48)):
						for pxy in range (376, 385):
							img.putpixel((pxx, pxy), pinkValue)
					dataPoint = np.array([0, 1])
					outputFile.write(str(dataPoint))
				# if the answer was no, write false to the file
				if yesno == '\'':
					dataPoint = np.array([1, 0])
					outputFile.write(str(dataPoint))
				# if the answer was lose, write something else to the file
				if yesno == 'l':
					for pxx in range ((86 + i*48), (132 + i*48)):
						for pxy in range (376, 385):
							img.putpixel((pxx, pxy), yellowValue)
					dataPoint = np.array([2, 2])
					outputFile.write(str(dataPoint))
			outputFile.write("\n")
			# then go through the z axis data
			for i in range(0, 12):
				# write white into the space below the 2 hour window we just opened (this is for relabeling images that have already been labeled)
				for pxx in range ((86 + i*48), (132 + i*48)):
					for pxy in range (563, 572):
						img.putpixel((pxx, pxy), whiteValue)
				# crop the 2 hour window and present it to the user for inspection
				imgCrop = img.crop((85 + i*48, 412, 133 + i*48, 563))
				imgCrop.resize((288, 900)).show()
				# prompt user for the label they wish to ascribe to the 2 hour window
				yesno = input('Did the image contain an anomaly? enter ; for yes \' for no or l for lose')
				while yesno != ';' and yesno != '\'' and yesno != 'l':
					yesno = input('Invalid input, please enter ; for yes or \' for no or l for lose')
				# if the answer was yes, edit the image and write true to file
				if yesno == ';':
					for pxx in range ((86 + i*48), (132 + i*48)):
						for pxy in range (563, 572):
							img.putpixel((pxx, pxy), pinkValue)
					dataPoint = np.array([0, 1])
					outputFile.write(str(dataPoint))
				# if the answer was no, write false to the file
				if yesno == '\'':
					dataPoint = np.array([1, 0])
					outputFile.write(str(dataPoint))
				# if the answer was lose, write something else to the file
				if yesno == 'l':
					for pxx in range ((86 + i*48), (132 + i*48)):
						for pxy in range (563, 572):
							img.putpixel((pxx, pxy), yellowValue)
					dataPoint = np.array([2, 2])
					outputFile.write(str(dataPoint))
			img.save(str(sys.argv[1]) + file)
		# if they did label whole day as non anomalous, write false 36 times
		else:
			for i in range(0, 3):
				for j in range(0, 12):
					outputFile.write(str(np.array([1, 0])))
				outputFile.write('\n')
		outputFile.close()