import argparse
import urllib.request
import os

ap = argparse.ArgumentParser()
ap.add_argument("--type", type=str, help="(required) choose either \"plots\" or \"ascii\". plots are required for createLabels. ascii are required for computeX. outputs to \"BAS_Data/raw_ascii/\" and \"BAS_Plots/images/\".")
ap.add_argument("--year_range", type=str, default="5-17", help="(optional) specify the range of years to download, ex: \"5-17\" would get 2005-2017 data of specified type.")
args = vars(ap.parse_args())

parentFilePath = os.path.abspath('')
pathDelims = ''
if '\\' in parentFilePath:
    pathDelims = '\\'
else:
    pathDelims = '/'
parentPathParts = parentFilePath.split(pathDelims)
parentFilePath = ''
for i in range(0, len(parentPathParts) - 1):
    parentFilePath += parentPathParts[i] + pathDelims

if args["type"] == None:
	print('error: file type to download not specified')
	exit()

years = range(int(args["year_range"].split("-")[0]), int(args["year_range"].split("-")[1]) + 1)

if "plots" in args["type"].lower():
	if not os.path.isdir(parentFilePath + 'BAS_Plots' + pathDelims + 'images' + pathDelims):
		os.makedirs(parentFilePath + 'BAS_Plots' + pathDelims + 'images' + pathDelims)
	for i in years:
		for j in range(1, 366):
			requestfile = 'hb' + str(i).zfill(2) + str(j).zfill(3) + '_1hz.gif'
			requesturl = 'http://psddb.nerc-bas.ac.uk/data/psddata/atmos/space/scm/halley/20' + str(i).zfill(2) + '/plots/' + requestfile
			requestdest = parentFilePath + 'BAS_Plots' + pathDelims + 'images' + pathDelims + requestfile
			print("requesting file " + requestfile + " ...")
			try:
				urllib.request.urlretrieve(requesturl, requestdest)
			except urllib.error.HTTPError:
				print("failed to retrieve file " + requestfile + " file might not exist")

if "ascii" in args["type"].lower():
	if not os.path.isdir(parentFilePath + 'BAS_Data' + pathDelims + 'raw_ascii' + pathDelims):
		os.makedirs(parentFilePath + 'BAS_Data' + pathDelims + 'raw_ascii' + pathDelims)
	for i in years:
		for j in range(1, 366):
			requestfile = str(j).zfill(3) + '20' + str(i).zfill(2) + '.TXT'
			requesturl = 'http://psddb.nerc-bas.ac.uk/data/psddata/atmos/space/scm/halley/data/ascii/' + requestfile
			requestdest = parentFilePath + 'BAS_Data' + pathDelims + 'raw_ascii' + pathDelims + requestfile
			print("requesting file " + requestfile + " ...")
			try:
				urllib.request.urlretrieve(requesturl, requestdest)
			except urllib.error.HTTPError:
				print("failed to retrieve file " + requestfile + " file might not exist")
