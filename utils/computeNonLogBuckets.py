# this is a program that takes in transformed BAS data and puts it into discrete buckets of a specified size
# the size of the bucket is how many integer values are placed into a given bucket. the operation is a simple integer division by the bucket size.

import os
import sys
import numpy as np
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("--input_directory", type=str, help="(required) the input directory of transformed values relative to repository root. ex: \"BAS_Data/parsed_ascii/transforms/\"")
ap.add_argument("--output_directory", type=str, help="(optional) the output directory for the bucket values relative to repository root. ex: \"BAS_Data/parsed_ascii/buckets/\", defaults to \"input_directory/buckets/\"")
ap.add_argument("--bucket_size", type=int, default=100000, help="(optional) the size of each discrete bucket into which the transform magnitudes are to be divided. minumum 1. defaults to 100000.")
args = vars(ap.parse_args())

# ensure that an input directory is specified
if args["input_directory"] == None:
    print('error: no input directory specified')
    exit()

# get the parent file path
parentFilePath = os.path.abspath('')
pathDelims = ''
if '\\' in parentFilePath:
    pathDelims = '\\'
else:
    pathDelims = '/'
parentPathParts = parentFilePath.split(pathDelims)
parentFilePath = ''
for i in range(0, len(parentPathParts) -1):
    parentFilePath += parentPathParts[i] + pathDelims

# check that the input directory exists
inputDirectory = parentFilePath + args["input_directory"]
if not os.path.isdir(inputDirectory):
    print('error: ' + inputDirectory + ' was not a directory')
    exit()

# check that the bucket output directory exists
if args["output_directory"] == None:
    outputDirectory = inputDirectory + 'buckets' + pathDelims
    if not os.path.isdir(outputDirectory):
        os.makedirs(outputDirectory)
else:
    outputDirectory = parentFilePath + args["output_directory"]
    if not os.path.isdir(outputDirectory):
        print('error: ' + outputDirectory + ' was not a directory')
        exit()

# parse the argument for the number of buckets into which we will divide the log base 10 values
if args["bucket_size"] < 1:
    print('error: number of buckets specified was less than 2')
    exit()
bucketSize = args["bucket_size"]

# list the files in the input directory
filesFound = os.listdir(inputDirectory)
print("[INFO] found a total of " + str(len(filesFound)) + " files.")

# keep track of the files we have translated and loop through them
filesTranslated = 1
for file in filesFound:
    # check that the file is a tet file
    if os.path.isfile(inputDirectory + file) and '.txt' in file.lower():
        print("[INFO] translating from file " + file + ", number " + str(filesTranslated) + " of " + str(len(filesFound)))
        # open the input file
        inputFile = open(inputDirectory + file)
        inputFile.readline()
        # open the output file
        outputFile = open(outputDirectory + file, 'w')
        outputFile.write("# the bucketized version of transformed data for file " + file + "\n")
        linesRead = 0
        # perform the computation and write to output file
        for line in inputFile:
            dat = line.split()
            if linesRead % 3 == 0:
                outputFile.write("X" + str(linesRead//3) + ": ")
            elif linesRead % 3 == 1:
                outputFile.write("Y" + str(linesRead//3) + ": ")
            elif linesRead % 3 == 2:
                outputFile.write("Z" + str(linesRead//3) + ": ")
            for i in range(1, len(dat)):
                val = np.float(dat[i])
                val = np.absolute(val)
                val = int(val // bucketSize)
                outputFile.write(str(val) + " ")
            outputFile.write("\n")
            linesRead = linesRead + 1
        # close the files, chalk up another file complete
        outputFile.close()
        inputFile.close()
        filesTranslated = filesTranslated + 1
