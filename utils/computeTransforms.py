# this program computes the spectrogram numeric data from a BAS file
# requires that "getData.py" has been run with option "--type ascii"
#
# compute the fourier transform of the differences between subsequent half second samples of magnetosphere data.
#
# inputs to fourier transform are signals of length <windowSize>, each point in the signal being one of the averaged intervals.
# the signal is advanced by <windowAdvance> samples for each computation of the fourier transform. signals may overlap 
# when <windowSize> is not equal to <windowAdvance>. first and last signals may or may not be the full length of <windowSize>.
#
# outputs from fourier transform is an array of length 512, each element of which represents the magnitude of a
# given frequency. there will be "n = (60s * 60m * 24h * 2samples/sec) / <windowAdvance> samples/window" signals of which the 
# fourier transform is taken. the transform will be taken for each of the X, Y, and Z axes that comprise the magnetometer data.
#
# this process will be done for all files in a given directory.

# import the necessary libraries
import os
import sys
import numpy as np
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("--input_directory", type=str, help="(required) the directory in which BAS ascii data has been downloaded relative to repository root. ex: \"BAS_Data/raw_ascii/\". note: requires \"python getData --type ascii\" to be run first.")
ap.add_argument("--output_directory", type=str, default="BAS_Data/parsed_ascii/transforms/", help="(optional) the directory to output data to relative to repository root. defaults to \"BAS_Data/parsed_ascii/transforms/\"")
ap.add_argument("--window_size", type=int, default=256, help="(optional) the size of the sampling window for each transform in seconds. defaults to 256 second windows.")
ap.add_argument("--window_advance", type=int, default=150, help="(optional) the distance to advance the sampling window after each transform, in seconds. defaults to 150 seconds, must not be larger than the window_size argument.")
args = vars(ap.parse_args())

# get the parent path file, or the root directory of this repository
parentFilePath = os.path.abspath('')
pathDelims = ''
if '\\' in parentFilePath:
    pathDelims = '\\'
else:
    pathDelims = '/'
parentPathParts = parentFilePath.split(pathDelims)
parentFilePath = ''
for i in range(0, len(parentPathParts) - 1):
    parentFilePath += parentPathParts[i] + pathDelims

# parse the argument for the input directory
if args["input_directory"] == None:
    print('error: input directory not specified')
    exit()
inputDirectory = parentFilePath + args["input_directory"]
if not os.path.isdir(inputDirectory):
    print('error: ' + inputDirectory + ' was not a directory')
    exit()
inputList = os.listdir(inputDirectory)

# parse the argument for the output directory
outputDirectory = parentFilePath + args["output_directory"]
if not os.path.isdir(outputDirectory):
    os.makedirs(outputDirectory)

# parse the argument for the window size and window advance
windowSize = args["window_size"] * 2
windowAdvance = args["window_advance"] * 2
if windowAdvance > windowSize:
    print('error: the window advance was larger than the window size. this would leave samples unaccounted for.')
    exit()

# track the number of files we have read
fileNum = 1

# iterate through the directory given as an argument
for file in inputList:
    if os.path.isfile(inputDirectory + file):
        print("[INFO] reading from file " + file + " number " + str(fileNum) + " of " + str(len(inputList)))
        # read in data from the text file
        inputFile = open(inputDirectory + file)
        inputFile.readline()
        inputFile.readline()
        rawData = inputFile.readlines()
        inputFile.close()
        # parse the data from the text file
        parsedData = np.zeros((4, 172800), dtype=np.float32)
        xVal = 0
        yVal = 0
        zVal = 0
        for i in range(0, len(rawData)):
            point = rawData[i].strip().split()
            if i != 0 and i % 5 == 0:
                parsedData[0][(i//5)-1] = point[0]
                parsedData[1][(i//5)-1] = xVal / 5
                parsedData[2][(i//5)-1] = yVal / 5
                parsedData[3][(i//5)-1] = zVal / 5
                xVal = np.float(point[1])
                yVal = np.float(point[2])
                zVal = np.float(point[3])
            elif i == len(rawData) - 1:
                parsedData[0][i//5] = point[0]
                parsedData[1][i//5] = (xVal + np.float32(point[1])) / 5
                parsedData[2][i//5] = (yVal + np.float32(point[2])) / 5
                parsedData[3][i//5] = (zVal + np.float32(point[3])) / 5
            else:
                xVal = xVal + np.float32(point[1])
                yVal = yVal + np.float32(point[2])
                zVal = zVal + np.float32(point[3])
        for i in range(0, len(rawData) // 5):
            if i < (len(rawData) // 5) - 1:
                parsedData[1][i] = parsedData[1][i+1] - parsedData[1][i]
                parsedData[2][i] = parsedData[2][i+1] - parsedData[2][i]
                parsedData[3][i] = parsedData[3][i+1] - parsedData[3][i]
            else:
                parsedData[1][i] = 0
                parsedData[2][i] = 0
                parsedData[3][i] = 0
        # define the window edges for each window, the length beyond the window advance which the
        # window extends as per the window size.
        windowEdges = (windowSize - windowAdvance) // 2
        # define the number of outputs based on this information
        if 172800 % windowAdvance == 0:
            numOutputs = 172800 // windowAdvance
        else:
            numOutputs = (172800 // windowAdvance) + 1
        # raise an error if our windows overlap by too much
        if windowEdges > windowAdvance:
            raise Exception('argument window_advance is too large for given argument window_size')

        # create the output file
        outputFile = open(outputDirectory + file, 'w')
        outputFile.write("Transformed data from " + file)

        # compute the fourier transform in x, y, and z axes for each of the windows.
        print("[INFO] computing transforms and writing to " + outputDirectory + file)
        for i in range(0, numOutputs):
            # determine the correct size of this particular window, accounting for trimming for windows that are
            # located at the start or end of day. also identify the beginning index for this particular window.
            if i == 0:
                n = windowSize - windowEdges
                windowBegin = 0
            elif i == (numOutputs - 1):
                n = windowSize - windowEdges - 1
                windowBegin = (i * windowAdvance) - windowEdges
            else:
                n = windowSize
                windowBegin = (i * windowAdvance) - windowEdges
            # create arrays to contain the signal from this particular window, in x y and z axes
            signalX = np.zeros((n), dtype=np.float32)
            signalY = np.zeros((n), dtype=np.float32)
            signalZ = np.zeros((n), dtype=np.float32)
            # parse the input into the arrays for this signal from this particular window
            for j in range(0, n):
                sampleNum = windowBegin + j
                if sampleNum < 172800:
                    signalX[j] = parsedData[1][sampleNum]
                    signalY[j] = parsedData[2][sampleNum]
                    signalZ[j] = parsedData[3][sampleNum]
            # transform the signals from this particular window
            transformX = np.fft.fft(signalX)
            transformY = np.fft.fft(signalY)
            transformZ = np.fft.fft(signalZ)
            # write the transformed data from this particular window to file
            outputFile.write("\nX" + str(i*windowAdvance) + ": ")
            for j in range(0, transformX.size):
                outputFile.write(str(transformX[j].real) + " ")
            outputFile.write("\nY" + str(i*windowAdvance) + ": ")
            for j in range(0, transformY.size):
                outputFile.write(str(transformY[j].real) + " ")
            outputFile.write("\nZ" + str(i*windowAdvance) + ": ")
            for j in range(0, transformZ.size):
                outputFile.write(str(transformZ[j].real) + " ")
        # close the file for this day, move on to the next one
        outputFile.close()
    fileNum = fileNum + 1
