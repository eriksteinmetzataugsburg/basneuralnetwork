from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers.core import Dropout

# Convolutional Nerual Network modeled after LeNet
class convnet:
	@staticmethod
	def build(width, height, depth, weightsPath=None):
		model = Sequential()
		model.add(Conv2D(20, (5, 5), padding="same", input_shape = (width, height, depth)))
		model.add(Activation("relu"))
		model.add(MaxPooling2D(pool_size = (4, 4), strides = (4, 4)))
		model.add(Conv2D(20, (5, 5), padding="same", input_shape = (width//4, height//4, depth)))
		model.add(Activation("relu"))
		model.add(MaxPooling2D(pool_size = (4, 4), strides = (4, 4)))
		model.add(Flatten())
		model.add(Dropout(rate=0.5))
		model.add(Dense(500))
		model.add(Activation("relu"))
		model.add(Dense(2))
		model.add(Activation("softmax"))

		if weightsPath is not None:
			model.load_weights(weightsPath)

		return model
