# this is a program to train neural networks using the keras framework
# the input data is expected to be BAS data as described elsewhere in this repository
# the BAS data must be retrieved using the utils before this program may be run
# if you are running this at Augsburg on babbage, the BAS data is already downloaded and transformed as of 2017-07-23

import argparse
from sklearn.model_selection import train_test_split
import numpy as np
import os
import sys
import importlib
import datetime
import time

# create an argument parser and load in the relevant arguments
ap = argparse.ArgumentParser()
ap.add_argument("--networks", type=str, help="(required) a list of networks to train. Ex: \"python trainsNetworks.py --networks convnet170705_1,convnet170705_2\". Taken from the /src/cnn/ directory sans \".py\" extension.")
ap.add_argument("--label_sets", type=str, help="(required) a list of label sets to train the networks on delimited by commas. Ex: \"python trainNetworks.py --label-sets set1,set0\". Taken from /BAS_Data/labels/ directory. Note: for elements which are present in multiple sets, preference will be given to first set in list in which the element occurs.")
ap.add_argument("--input_data", type=str, help="(required) directory to take input data from, via repository root. Ex: \"python trainNetworks.py --input_data BAS_Data/parsed_ascii/buckets/\". Note: must run utils to download and process input data before running this program if not on babbage.")
ap.add_argument("--optimizer", type=str, help="(required) the optimizer to train with. Options are \"SGD\" \"RMSprop\" \"Adagrad\" \"Adadelta\" \"Adam\" \"Adamax\" and \"Nadam\"")
ap.add_argument("--learning_rate", type=float, default=-1, help="(optional) the learning rate for the optimizer. Float gte 0. If left empty or set to a negative value, uses the keras default value for specified optimizer.")
ap.add_argument("--loss_function", type=str, default='categorical_crossentropy', help="(optional) the loss function for the evaluation of the network. Default value is \"categorical_crossentropy\". See keras documentation for more options.")
ap.add_argument("--metric", type=str, default='accuracy', help="(optional) the metric by which the training of the network is evaluated. Default value is \"accuracy\". See keras documentation for more options.")
ap.add_argument("--sessions", type=int, default=10, help="(optional) number of sessions to train each network. sessions are each time the network is instantiated and trained. this corresponds to unique sets of weights created. defaults to 10")
ap.add_argument("--epochs", type=int, default=20, help="(optional) number of epochs per training session. an epoch is a forward and backward pass of the each data point in the training set. defaults to 20")
ap.add_argument("--batch_size", type=int, default=128, help="(optional) number of inputs to use for each training or evaluation batch. an batch is the number of data points in each for forward and backward pass. defaults to 128")
ap.add_argument("--shuffle", type=int, default=1, help="(optional) enter positive integer to shuffle data between training epochs, negative integer to not shuffle. defaults to shuffle.")
ap.add_argument("--train_test_split", type=float, default=0.25, help="(optional) enter float value between 0.1 and 0.5. this is the percentage of the data to reserve for testing, the other portion being the percentage of data used to train the network. default is 0.25.")
args = vars(ap.parse_args())

# get the parent path file, or the root directory of this repository
parentFilePath = os.path.abspath('')
pathDelims = ''
if '\\' in parentFilePath:
    pathDelims = '\\'
    parentPathParts = parentFilePath.split('\\')
    parentFilePath = ''
    for i in range(0, len(parentPathParts) - 1):
        parentFilePath += parentPathParts[i] + '\\'
else:
    pathDelims = '/'
    parentPathParts = parentFilePath.split('/')
    parentFilePath = ''
    for i in range(0, len(parentPathParts) - 1):
        parentFilePath += parentPathParts[i] + '/'

# parse the argument for the networks
if args["networks"] == None:
    print('error: no networks argument was provided')
    exit()
networkStrings = args["networks"].split(",")
networkObjects = list()
for i in range(0, len(networkStrings)):
    try:
        networkObjects.append(getattr(importlib.import_module('cnn.' + networkStrings[i]), 'convnet'))
    except:
        print('error: network ' + networkStrings[i] + ' was not found in directory \"src/cnn/\" or network description class \"convnet\" was not present in ' + networkStrings[i])
        exit()

# parse the argument for the label sets we will be loading in
if args["label_sets"] == None:
    print('error: no label sets specified')
    exit()
labelDirectoryStrings = args["label_sets"].split(",")
labelDirectories = list()
for i in range(0, len(labelDirectoryStrings)):
    labelDirectories.append(parentFilePath + 'BAS_Data' + pathDelims + 'labels' + pathDelims + labelDirectoryStrings[i] + pathDelims)
    if not os.path.isdir(labelDirectories[i]):
        print('error: set path \"' + labelDirectories[i] + '\" was not a directory')
        exit()

# parse the argument for the input data directory
if args["input_data"] == None:
    print('error: no input data directory specified')
    exit()
dataDirectory = parentFilePath + args["input_data"]
if not os.path.isdir(dataDirectory):
    print('error: input data directory not found')
    exit()

# parse the argument for the optimizer
optimizerName = args["optimizer"]
if not optimizerName in ['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']:
    print('error: optimizer not recognized. check spelling')
    exit()
optimizer = getattr(getattr(importlib.import_module('keras'), 'optimizers'), optimizerName)

# parse the argument for the learning rate
learningRate = args["learning_rate"]

# parse the argument for the loss function
lossFunctionName = args["loss_function"]
if not lossFunctionName in ['mean_squared_error', 'mean_absolute_error', 'mean_absolute_percentage_error', 'mean_squared_logarithmic_error', 'squared_hinge', 'hinge', 'categorical_hinge', 'logcosh', 'categorical_crossentropy', 'sparse_categorical_crossentropy', 'binary_crossentropy', 'kullback_leibler_divergence', 'poisson', 'cosine_proximity']:
    print('error: loss function was not in keras as of 2017-07-23')
    exit()

# parse the argument for the metric
metricName = args["metric"]
if not metricName in ['accuracy', 'binary_accuracy', 'categorical_accuracy', 'sparse_categorical_accuracy', 'top_k_categorical_accuracy', 'sparse_top_k_categorical_accuracy']:
    print('error: metric was not in keras as of 2017-07-23')
    exit()

# parse the argument for the number of sessions
numSessions = args["sessions"]
if numSessions < 1:
    print('error: sessions must be a non-zero positive integer')	
    exit()

# parse the argument for the number of epochs
numEpochs = args["epochs"]
if numEpochs < 1:
    print('error: epochs must be a non-zero positive integer')
    exit()

# parse the argument for the batch size
batchSize = args["batch_size"]
if batchSize < 1:
    print('error: batch size must be a non-zero positive integer')
    exit()

# parse the argument for shuffling
shuffleValue = args["shuffle"]
if shuffleValue < 0:
    shuffleValue = False
else:
    shuffleValue = True

# parse the argument for test and train split
testSize = args["train_test_split"]
if testSize < 0.1 or testSize > 0.5:
    print('error: test size was not between 0.1 and 0.5')
    exit()

print("[INFO] reading data...")

# create a list of label and data files from arguments
# only adds files that have both a label file and a data file
labelAdded = list()
labelPaths = list()
dataAvail = os.listdir(dataDirectory)
dataPaths = list()
# loop through the label directories
for labelDirectory in labelDirectories:
    labelFiles = os.listdir(labelDirectory)
    # loop through the files in label directory
    for file in labelFiles:
        # if the label hasn't been added yet and there is data available, add label and data files to lists
        if not file in labelAdded:
            dataFileName = file[2:5] + "20" + file[0:2] + ".TXT"
            if dataFileName in dataAvail:
                labelAdded.append(file)
                labelPaths.append(labelDirectory + file)
                dataPaths.append(dataDirectory + dataFileName)

# create empty lists into which we will read the labels and data
labelList = list()
dataList = list()

# double check our file list lengths as per paranoid android
# if you haven't heard paranoid android, it is a marvelous song by radiohead (reddit cliques be damned)
if len(labelPaths) != len(dataPaths):
    print('error: there were days for which labels and data did not both exist')
    exit()

# loop through each pair of files, reading the data into the lists
for fileNum in range(0, len(labelPaths)):
    print("[INFO] reading in file " + str(fileNum + 1) + " of " + str(len(labelPaths)) + "...")
    # check if the label file does exist and read it in
    if os.path.isfile(labelPaths[fileNum]):
        labelFile = open(labelPaths[fileNum])
    else:
        raise Exception("couldn't find file " + labelPaths[fileNum])
    labelLines = labelFile.readlines()
    labelFile.close()
    # check if the data file does exist
    if os.path.isfile(dataPaths[fileNum]):
        dataFile = open(dataPaths[fileNum])
    else:
        raise Exception("couldn't find file " + dataPaths[fileNum])
    dataFile.readline()
    # begin reading in the data file, adding the relevant part of the label file as we go through it
    # data file is read in line by line as it is pretty damned big
    # we read in 2 hour windows at a time across 3 axes, so 12 hours per day
    for i in range(0, 12):
        # read in the 2 hour window for each axis, x y and z
        # scale the inputs in case the user has specified a transform that does not
        # decompose into 256 postive and 256 negative frequencies.
        anInput = np.zeros((3, 48, 256), dtype=np.float32)
        for j in range(0, 48):
            anXPoint = dataFile.readline().split()
            xOffset = len(anXPoint) // 2 // 256
            if xOffset < 1:
                xOffset = 1
            anYPoint = dataFile.readline().split()
            yOffset = len(anYPoint) // 2 // 256
            if yOffset < 1:
                yOffset = 1
            anZPoint = dataFile.readline().split()
            zOffset = len(anZPoint) // 2 // 256
            if zOffset < 1:
                zOffset = 1
            for k in range(1, len(anXPoint) // 2):
                anInput[0][j][k-1] = anXPoint[k*xOffset]
            for k in range(1, len(anYPoint) // 2):
                anInput[1][j][k-1] = anYPoint[k*yOffset]
            for k in range(1, len(anZPoint) // 2):
                anInput[2][j][k-1] = anZPoint[k*zOffset]
        # define the relevant portion of the label file
        substrstart = 1 + i*5
        substrend = 4 + i*5
        # grab the relevant portion of the label file for the x axis
        labelArray = labelLines[3][substrstart:substrend].split()
        # if the relevant label does not tell us to ignore this data, add it to label and data lists
        if int(labelArray[0]) == 0 or int(labelArray[1]) == 0:
            labelList.append(labelArray)
            anXInput = np.zeros((48, 256), dtype=np.float32)
            for j in range(0, 48):
                for k in range(0, 256):
                    anXInput[j][k] = anInput[0][j][k]
            dataList.append(anXInput)
        # grab the relevant portion of the label file for the y axis
        labelArray = labelLines[4][substrstart:substrend].split()
        # if the relevant label does not tell us to ignore this data, add it to label and data lists
        if int(labelArray[0]) == 0 or int(labelArray[1]) == 0:
            labelList.append(labelArray)
            anYInput = np.zeros((48, 256), dtype=np.float32)
            for j in range(0, 48):
                for k in range(0, 256):
                    anYInput[j][k] = anInput[1][j][k]
            dataList.append(anYInput)
        # grab the relevant portion of the label file for the z axis
        labelArray = labelLines[5][substrstart:substrend].split()
        # if the relevant label does not tell us to ignore this data, add it to label and data lists
        if int(labelArray[0]) == 0 or int(labelArray[1]) == 0:
            labelList.append(labelArray)
            anZInput = np.zeros((48, 256), dtype=np.float32)
            for j in range(0, 48):
                for k in range(0, 256):
                    anZInput[j][k] = anInput[2][j][k]
            dataList.append(anZInput)

# Transfer the data from the list to a numpy array
data = np.zeros((len(dataList), 48, 256), dtype=np.float32)
for i in range(0, len(dataList)):
    if i % 1000 == 0:
        print("[INFO] translating input " + str(i + 1) + " of " + str(len(dataList)))
    currentSeg = dataList[i]
    for j in range(0, 48):
        for k in range(0, 256):
            data[i][j][k] = currentSeg[j][k]

# Transfer the labels from the list to a numpy array
labels = np.zeros((len(labelList), 2), dtype=int)
for i in range(0, len(dataList)):
    if i % 1000 == 0:
        print("[INFO] translating label " + str(i + 1) + " of " + str(len(dataList)))
    currentSeg = labelList[i]
    labels[i][0] = currentSeg[0]
    labels[i][1] = currentSeg[1]

# Add another axis to the data array. This is done as Keras expects a 2D image with a number of color channels for 2D Convolutions.
# We simply tell Keras with this that there is only one "color" channel, in our data one channel of power data.
data = data[:, :, :, np.newaxis]

# declare optimizer
if learningRate < 0:
    opt = optimizer()
    learningRate = 'default, see keras documentation'
else:
    opt = optimizer(lr=learningRate)

# get the highest enumeration of results directories for today's date
todaysDate = str(datetime.date.today())
extantResults = os.listdir(parentFilePath + 'results' + pathDelims)
enum = -1
for file in extantResults:
    if todaysDate in file:
        thisEnum = int(file[len(todaysDate)+4:])
        if thisEnum > enum:
            enum = thisEnum
# increment the enumeration by one
enum = enum + 1
# declare the directory name for our results
resultsDirectory = parentFilePath + 'results' + pathDelims + todaysDate + '_run' + str(enum) + pathDelims

print("[INFO] writing results to directory " + resultsDirectory)
# create the directories for the results
os.makedirs(resultsDirectory + 'weights' + pathDelims)
# open file to write all accuracy results to
allNetworksOutput = open(resultsDirectory + 'allresults', 'w')
# create a string containing the details of the training sessions
trainingParams = "\nNetworks=" + str(networkStrings) + "\nInput labels=" + str(args["label_sets"]) + "\nInput data=" + str(dataDirectory)
trainingParams += "\nNumber Inputs=" + str(len(labelPaths)) + "\nOptimizer=" + str(optimizerName) + "\nLearning Rate=" + str(learningRate)
trainingParams += "\nLoss Function=" + str(lossFunctionName) + "\nMetric=" + str(metricName) + "\nNumber of sessions=" + str(numSessions)
trainingParams += "\nNumber of epochs=" + str(numEpochs) + "\nBatch Size=" + str(batchSize) + "\nShuffle=" + str(shuffleValue)
trainingParams += "\nTrain test split=" + str(testSize) + "\n"
# write training session details to all outputs file
allNetworksOutput.write("Trained networks using:" + trainingParams)
for i in range(0, len(networkObjects)):
    # write network name to all outputs file
    allNetworksOutput.write("Results for network " + networkStrings[i] + "\n")
    # open file to write accuracy results for this particular network to
    thisNetworkOutput = open(resultsDirectory + networkStrings[i] + 'results', 'w')
    # write training session details to this networks outputs file
    thisNetworkOutput.write("Trained network using:" + trainingParams)
    # fit aka train this particular network the specified number of times
    for j in range(0, numSessions):
        # inform the user of this
        print("[INFO] training " + networkStrings[i] + "...")
        print("[INFO] training instance " + str(i*numSessions + j) + " of " + str(numSessions*len(networkObjects)) + "...")
        # build this particular network then compile it using specified loss function, optimizer, and metric
        model = networkObjects[i].build(height=256, width=48, depth=1)
        model.compile(loss=lossFunctionName, optimizer=opt, metrics=[metricName])
        # split the data and labels into training and testing sets. i chose random state 42 as static.
        (trainData, testData, trainLabels, testLabels) = train_test_split(data, labels, test_size=testSize, random_state=42)
        # train aka fit this particular network instance
        model.fit(x=trainData, y=trainLabels, batch_size=batchSize, epochs=numEpochs, verbose=1, shuffle=shuffleValue)
        # test aka evaluate this particular network instance and write results to screen and file
        print("[INFO] evaluating...")
        (loss, accuracy) = model.evaluate(testData, testLabels, batch_size=batchSize, verbose=1)
        time.sleep(0.5)
        print("[INFO] accuracy: " + str(accuracy))
        allNetworksOutput.write("\trun " + str(j) + ": " + str(accuracy) + "\n")
        thisNetworkOutput.write("\trun " + str(j) + ": " + str(accuracy) + "\n")
        # save this particular network instance's weights to file
        # save is of the format h5py, a python based database system. it does not appear to have managed access. i do not know why it exists.
        print("[INFO] saving weights to file " + resultsDirectory + pathDelims + 'weights' + pathDelims + networkStrings[i] + 'session' + str(j))
        model.save(resultsDirectory + pathDelims + 'weights' + pathDelims + networkStrings[i] + 'session' + str(j))
    # close the output file for this particular network
    thisNetworkOutput.close()
# close the output file for this particular run
allNetworksOutput.close()
