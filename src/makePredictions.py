import os
import sys
import numpy as np
import argparse
import importlib

# make an argument parser that will allow us to specify the network to evaluate, the weights file to evaluate on, and the input data directory and the type of input file
ap = argparse.ArgumentParser()
ap.add_argument("--network", type=str, help="(required) the network which will be used to predict if there is an anomaly in the input data. Ex: \"convnet170705_1\"")
ap.add_argument("--weights", type=str, help="(required) path to the weights file to use with the network via repository root. Ex: \"results/170723_num0/weights/convnet170705_1session5\". Please ensure weights matches network, there is no error handling for mismatch.")
ap.add_argument("--input_data", type=str, help="(required) path to the directory of files or individual file to predict if there is an anomaly in, via repository root. Ex: \"BAS_Data/parsed_ascii/buckets/\" or \"BAS_Data/parsed_ascii/buckets/0422007.TXT\". Note: subdirectories will be ignored.")
ap.add_argument("--input_type", type=str, help="(required) type of file to predict if there is an anomaly in. Options are \"BAS\" or \"transformed\". BAS option is for 1 hz ascii text download for the BAS Halley SCM site from the BAS website. Transformed option includes any of the outputs from the utils of this package (transforms, buckets, log10s, log10buckets).")
args = vars(ap.parse_args())

# get the parent path file, or the root directory of this repository
parentFilePath = os.path.abspath('')
pathDelims = ''
if '\\' in parentFilePath:
    pathDelims = '\\'
    parentPathParts = parentFilePath.split('\\')
    parentFilePath = ''
    for i in range(0, len(parentPathParts) - 1):
        parentFilePath += parentPathParts[i] + '\\'
else:
    pathDelims = '/'
    parentPathParts = parentFilePath.split('/')
    parentFilePath = ''
    for i in range(0, len(parentPathParts) - 1):
        parentFilePath += parentPathParts[i] + '/'

# parse the argument for network
if args["network"] == None:
    print('error: no network specified')
    exit()
try:
    convnet = getattr(importlib.import_module('cnn.' + args["network"]), 'convnet')
    model = convnet.build(width=48, height=256, depth=1)
except:
    print('error: network ' + args["network"] + ' was not found in directory \"src/cnn/\" or network description class \"convnet\" was not present in ' + args["network"])
    exit()

# parse the argument for the weights file
if args["weights"] == None:
    print('error: no weights file specified')
    exit()
if not os.path.isfile(parentFilePath + args["weights"]):
    print('error: weights file ' + args["weights"] + ' does not exist.')
    exit()
try:
    model.load_weights(parentFilePath + args["weights"], by_name=False)
except:
    print('error: weights file did not match the network specified')
    exit()

# parse the argument for the input data
inputFiles = list()
if args["input_data"] == None:
    print('error: no input data specified')
    exit()
if not os.path.isdir(parentFilePath + args["input_data"]):
    inputFiles.append(parentFilePath + args["input_data"])
elif os.path.isdir(parentFilePath + args["input_data"]):
    for file in os.listdir(parentFilePath + args["input_data"]):
        if not os.path.isdir(parentFilePath + args["input_data"] + file):
            inputFiles.append(parentFilePath + args["input_data"] + file)
else:
    print('error: path to input data ' + args["input_data"] + ' does not exist.')
    exit()

# parse the argument for the input type
if args["input_type"] == None:
    print('error: input type not specified')
    exit()
if not args["input_type"] in ["BAS", "bas", "Transformed", "transformed"]:
    print('error: input type not recognized. options are \"BAS\" or \"Transformed\"')
    exit()
inputType = args["input_type"]

# make an empty list for the days of data we will be predicting on
inputData = list()
# make an empty list for the names of the days we will be predicting on
inputNames = list()

# keep track of how many files we have read
fileNum = 1

# if we are predicting on raw BAS data, compress it into half second intervals, take the
# differences between intervals, and compute fourier transforms with sample windows of size
# 512 advancing the sample window 300 samples after each transform.
if inputType == 'BAS' or inputType == 'bas':
    # loop through input files
    for inputFile in inputFiles:
        print("[INFO] reading in file " + str(fileNum) + " of " + str(len(inputFiles)) + "...")
        # track the name, prepare the file for reading
        inputNames.append(inputFile)
        inputFile = open(inputFile)
        inputFile.readline()
        inputFile.readline()
        rawData = inputFile.readlines()
        inputFile.close()
        # allocate space for the data to go
        inputArray = np.zeros((3, 172800), dtype=np.float32)
        # start counting so we can take the average of 5 data points in x y and z
        xVal = 0
        yVal = 0
        zVal = 0
        # parse the file
        for i in range(0, len(rawData)):
            # fashion this line into a list
            point = rawData[i].strip().split()
            # if we are on every 5th line, take the average and put it in the array
            if i != 0 and i % 5 == 0:
                inputArray[0][(i//5)-1] = xVal / 5
                inputArray[1][(i//5)-1] = yVal / 5
                inputArray[2][(i//5)-1] = zVal / 5
                xVal = np.float(point[1])
                yVal = np.float(point[2])
                zVal = np.float(point[3])
            # if we are at the end of the file, add and line and write last average
            elif i == len(rawData) - 1:
                inputArray[0][i//5] = (xVal + np.float32(point[1])) / 5
                inputArray[1][i//5] = (yVal + np.float32(point[2])) / 5
                inputArray[2][i//5] = (zVal + np.float32(point[3])) / 5
            # otherwise keep adding to the values
            else:
                xVal = xVal + np.float(point[1])
                yVal = yVal + np.float(point[2])
                zVal = zVal + np.float(point[3])
        # get differences, leaving the last data point in x y and z empty
        for i in range(0, 172800):
            if i < 172799:
                inputArray[0][i] = inputArray[0][i+1] - inputArray[0][i]
                inputArray[1][i] = inputArray[1][i+1] - inputArray[1][i]
                inputArray[2][i] = inputArray[2][i+1] - inputArray[2][i]
            else:
                inputArray[0][i] = 0
                inputArray[1][i] = 0
                inputArray[2][i] = 0
        parsedArray = np.zeros((36,48,256), dtype=np.float32)
        for i in range(0, 576):
            if i == 0:
                n = 406
            elif i == 575:
                n = 300
            else:
                n = 512
            signalX = np.zeros((n), dtype=np.float32)
            signalY = np.zeros((n), dtype=np.float32)
            signalZ = np.zeros((n), dtype=np.float32)
            windowBegin = i*300
            windowEnd = i*300 + n
            for j in range(windowBegin, windowEnd):
                signalX[j-windowBegin] = inputArray[0][j]
                signalY[j-windowBegin] = inputArray[1][j]
                signalZ[j-windowBegin] = inputArray[2][j]
            transformX = np.fft.fft(signalX, 512)
            transformY = np.fft.fft(signalY, 512)
            transformZ = np.fft.fft(signalZ, 512)
            # come back for this, adjust based on how the new buckets do.
            for j in range(1, 256):
                xVal = np.absolute(transformX[j]) // 100000
                yVal = np.absolute(transformY[j]) // 100000
                zVal = np.absolute(transformZ[j]) // 100000
                parsedArray[i//48][i%48][j] = xVal
                parsedArray[(i//48)*2][i%48][j] = zVal
                parsedArray[(i//48)*3][i%48][j] = yVal
        # add the data we have transformed to the data list
        inputData.append(parsedArray)
        # continue tracking the number of files we have read
        fileNum = fileNum + 1
# only need to arrange the data in arrays if we are reading in data that has already been transformed.
elif inputType == 'Transformed' or inputType == 'transformed':
    for inputFile in inputFiles:
        print("[INFO] reading in file " + str(fileNum) + " of " + str(len(inputFiles)) + "...")
        # keep track of the name of the files we are reading in
        inputNames.append(inputFile)
        # read in the file
        inputFile = open(inputFile)
        inputFile.readline()
        fileLines = inputFile.readlines()
        inputFile.close()
        # allocate space for storing the read data
        parsedArray = np.zeros((36,48,256), dtype=np.float32)
        # read the data
        for i in range(0, len(fileLines)):
            fileLine = fileLines[i].split()
            for j in range(2, 257):
                if i % 3 == 0:
                    parsedArray[i//144][(i%144)//3][j-2] = np.float32(fileLine[j])
                if i % 3 == 1:
                    parsedArray[(i//144) + 12][(i%144)//3][j-2] = np.float32(fileLine[j])
                if i % 3 == 2:
                    parsedArray[(i//144) + 24][(i%144)//3][j-2] = np.float32(fileLine[j])
        inputData.append(parsedArray)
        fileNum = fileNum + 1

for i in range(0, len(inputData)):
    print("[RES] predicting for file " + inputNames[i])
    prediction = model.predict(inputData[i][:,:,:,np.newaxis], verbose=0)
    noHits = True
    for j in range(0, len(prediction)):
        if prediction[j][1] > .5:
            noHits = False
            if j // 12 == 0:
                print("\tthe model predicts there is an anomaly in the x axis between " + str((j%12)*2) + ":00 and " + str(((j%12)+1)*2) + ":00, probability " + str(100*prediction[j][1]) + "%")
            if j // 12 == 1:
                print("\tthe model predicts there is an anomaly in the y axis between " + str((j%12)*2) + ":00 and " + str(((j%12)+1)*2) + ":00, probability " + str(100*prediction[j][1]) + "%")
            if j // 12 == 2:
                print("\tthe model predicts there is an anomaly in the z axis between " + str((j%12)*2) + ":00 and " + str(((j%12)+1)*2) + ":00, probability " + str(100*prediction[j][1]) + "%")
    if noHits:
        print("\tthere do not appear to be any anomalies in the file")
